/*
 * linux/arch/arm/mach-at91/uart.c
 *
 *  Copyright (C) 2008 Pieter du Preez <pdupreez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include "uart.h"

#include <linux/platform_device.h>
#include <mach/board.h>
#include "generic.h"

struct platform_device *atmel_default_console_device;

static struct platform_device *__initdata at91_uarts[ATMEL_MAX_UART];

static struct at91_uart {
	struct atmel_uart_data data;
	u64 dmamask;
	struct platform_device devices;
	struct resource resources[2];
} uart[ATMEL_MAX_UART];

void __init at91_add_device_serial(void)
{
	int i;

	for (i = 0; i < ATMEL_MAX_UART; i++) {
		if (at91_uarts[i])
			platform_device_register(at91_uarts[i]);
	}

	if (!atmel_default_console_device)
		printk(KERN_INFO "AT91: No default serial console defined.\n");
}

struct platform_device * __init get_at91_uart_device_ptr(int i)
{
	struct platform_device *pdev;
	if (i < 0 || i >= ATMEL_MAX_UART)
		return 0;

	pdev = &uart[i].devices;
	pdev->name = "atmel_usart";
	pdev->id = i;
	pdev->dev.dma_mask = &uart[i].dmamask;
	pdev->dev.coherent_dma_mask = DMA_BIT_MASK(32);
	pdev->dev.platform_data = &uart[i].data;
	pdev->resource = uart[i].resources;
	pdev->num_resources = ARRAY_SIZE(uart[i].resources);

	return pdev;
}

static void __init at91_register_uart(int device_number, unsigned port_number,
	void (*configure_usart_pins)(void), const char *clock_name)
{
	struct platform_device *pdev;

	if (port_number < 0)
		return;

	if (port_number < ATMEL_MAX_UART) {
		pdev = get_at91_uart_device_ptr(device_number);
		(configure_usart_pins)();
		pdev->id = port_number;
		at91_clock_associate(clock_name, &pdev->dev, "usart");
		at91_uarts[port_number] = pdev;
	}
}

void __init at91_init_uarts(void)
{
	int i;

	for (i = 0; i < ATMEL_MAX_UART; i++) {
		at91_uarts[i] = 0;

		uart[i].resources[0].start	= usart_base_addr[i];
		uart[i].resources[0].end	= usart_base_addr[i];
		uart[i].resources[0].flags	= IORESOURCE_MEM;

		uart[i].resources[1].start	= usart_id_addr[i];
		uart[i].resources[1].end	= usart_id_addr[i];
		uart[i].resources[1].flags	= IORESOURCE_IRQ;

		uart[i].dmamask = DMA_BIT_MASK(32);

		if (i == 0) {
			uart[i].resources[0].end += SZ_512 - 1;

			/* DBGU not capable of receive DMA */
			uart[i].data.use_dma_tx = 0;
			uart[i].data.use_dma_rx = 0;
			uart[i].data.regs =
				(void __iomem *)(AT91_VA_BASE_SYS + AT91_DBGU);
		} else {
			uart[i].resources[0].end += SZ_16K - 1;

			uart[i].data.use_dma_tx = 1;
			uart[i].data.use_dma_rx = 1;
		}
	}

	if (ATMEL_MAX_UART > 0)
		at91_register_uart(0, CONFIG_AT91_DBGU_DEV_NR,
				   configure_dbgu_pins, "mck");
	if (ATMEL_MAX_UART > 1)
		at91_register_uart(1, CONFIG_AT91_USART0_DEV_NR,
				   configure_usart0_pins, "usart0_clk");
	if (ATMEL_MAX_UART > 2)
		at91_register_uart(2, CONFIG_AT91_USART1_DEV_NR,
				   configure_usart1_pins, "usart1_clk");
	if (ATMEL_MAX_UART > 3)
		at91_register_uart(3, CONFIG_AT91_USART2_DEV_NR,
				   configure_usart2_pins, "usart2_clk");
	if (ATMEL_MAX_UART > 4)
		at91_register_uart(4, CONFIG_AT91_USART3_DEV_NR,
				   configure_usart3_pins, "usart3_clk");
	if (ATMEL_MAX_UART > 5)
		at91_register_uart(5, CONFIG_AT91_USART4_DEV_NR,
				   configure_usart4_pins, "usart4_clk");
	if (ATMEL_MAX_UART > 6)
		at91_register_uart(6, CONFIG_AT91_USART5_DEV_NR,
				   configure_usart5_pins, "usart5_clk");

	if (CONFIG_AT91_DBGU_DEV_NR < ATMEL_MAX_UART)
		atmel_default_console_device = at91_uarts[0];
}
