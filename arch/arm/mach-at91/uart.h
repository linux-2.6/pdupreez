/*
 * linux/arch/arm/mach-at91/uart.c
 *
 *  Copyright (C) 2008 Pieter du Preez <pdupreez@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include <linux/dma-mapping.h>

/*
 * As we the following defines are checked by the compiler,
 * we need to assign some dummy values to them, as they may
 * not be defined by Kconfig.
 */

#ifndef CONFIG_AT91_DBGU_DEV_NR
#define CONFIG_AT91_DBGU_DEV_NR -1
#endif

#ifndef CONFIG_AT91_USART0_DEV_NR
#define CONFIG_AT91_USART0_DEV_NR -1
#endif

#ifndef CONFIG_AT91_USART1_DEV_NR
#define CONFIG_AT91_USART1_DEV_NR -1
#endif

#ifndef CONFIG_AT91_USART2_DEV_NR
#define CONFIG_AT91_USART2_DEV_NR -1
#endif

#ifndef CONFIG_AT91_USART3_DEV_NR
#define CONFIG_AT91_USART3_DEV_NR -1
#endif

#ifndef CONFIG_AT91_USART4_DEV_NR
#define CONFIG_AT91_USART4_DEV_NR -1
#endif

#ifndef CONFIG_AT91_USART5_DEV_NR
#define CONFIG_AT91_USART5_DEV_NR -1
#endif
